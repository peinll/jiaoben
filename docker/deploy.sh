#!/bin/bash

#项目名称
projectName=$1
#Dockerfile
dockerfile=$2
#端口
port=$3
#本机公网ip
ip=$(curl https://ipv4.ipw.cn/api/ip/myip)
#部署通知url
monitorUrl='http://115.238.129.217:8858/jenkins/deploy'
#镜像仓库
repo=115.238.129.217:5000
#镜像标签
tag=$(date -d today +"%Y%m%d%H%M%S")

alias tmp="cd /opt/web/$projectName"
echo "工作目录：/opt/web/"$projectName
tmp
echo "开始构建Docker镜像"
docker build -f ./$dockerfile  -t $projectName:$tag .
echo "Docker镜像构建完毕"
echo "开始异步上传镜像仓库"
docker tag $projectName:$tag $repo/$projectName:$tag &
docker push $repo/$projectName:$tag &
echo "开始启动Docker容器"
curl $monitorUrl'/start?projectName='$projectName'&server='$ip
docker stop $projectName
docker rm $projectName
docker run --add-host appapi2.gamersky.com:192.168.0.185 --add-host i.gamersky.com:192.168.0.197 --add-host cm.gamersky.com:192.168.0.190 --add-host cm1.gamersky.com:192.168.0.190 --name=$projectName --restart=on-failure:5  -e TZ="Asia/Shanghai" -d -v /opt/web/logs:/logs -v /opt/web/$projectName/config:/config -p $port:$port $projectName:$tag
result=$(curl --connect-timeout 60 -m 60 $monitorUrl'/success?projectName='$projectName'&port='$port'&server='$ip)
echo $result
