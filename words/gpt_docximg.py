from docxtpl import DocxTemplate, InlineImage
from PIL import Image
import os

# 定义docx模板路径和图片文件夹路径
template_path = 'path/to/template.docx'
image_folder_path = 'path/to/image/folder/'

# 打开docx模板文件
doc = DocxTemplate(template_path)

# 获取模板中所有表格
tables = doc.tables

# 遍历每个表格
for table in tables:
    rows = table.rows
    for row in rows:
        cells = row.cells
        for cell in cells:
            # 遍历每个单元格
            for paragraph in cell.paragraphs:
                # 获取每个段落中的所有图片
                for run in paragraph.runs:
                    if run.text.startswith('image:'):
                        # 获取图片文件名
                        image_name = run.text[6:]
                        # 构造图片文件路径
                        image_path = os.path.join(image_folder_path, image_name)
                        # 打开图片文件
                        img = Image.open(image_path)
                        # 将图片文件转换为InlineImage对象
                        inline_image = InlineImage(doc, image_path, img.width, img.height)
                        # 替换段落中的图片占位符
                        run.text = ''
                        run.add_picture(inline_image)

# 保存为新的docx文件
doc.save('path/to/output.docx')