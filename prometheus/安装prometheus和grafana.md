安装prometheus

```shell
docker run -d --restart=always --name prometheus -p 9090:9090 \
-v /opt/prometheus/conf/prometheus.yml:/etc/prometheus/prometheus.yml \
-v /opt/prometheus/data/:/prometheus \
-v /opt/prometheus/conf/rules:/usr/local/prometheus/rules \
prom/prometheus
```

或

```shell
docker run --name prometheus -d -p 9090:9090 --restart=always \
-v /opt/prometheus/conf/prometheus.yml:/etc/prometheus/prometheus.yml \
-v /opt/prometheus/data/:/prometheus \
-v /opt/prometheus/conf/rules:/usr/local/prometheus/rules \
prom/prometheus --config.file=/etc/prometheus/prometheus.yml --web.enable-lifecycle
```





#安装 cadvisor 监控docker性能

```shell
docker run -d --restart=always  --name cadvisor -p 9080:8080 \
-v /:/rootfs:ro \
-v /var/run:/var/run:rw \
-v /sys:/sys:ro \
-v /var/lib/docker/:/var/lib/docker:ro \
-v /dev/disk/:/dev/disk:ro \
-v /etc/localtime:/etc/localtime:rw \
google/cadvisor
```

#安装node-exporter 监控linux

```shell
docker run -d --restart=always --name node-exporter -p 9100:9100 \
-v /proc:/host/proc:ro \
-v /sys:/host/sys:ro \
-v /:/rootfs:ro \
-v /etc/localtime:/etc/localtime:rw \
prom/node-exporter \
--path.procfs /host/proc \
--path.sysfs /host/sys \
--collector.filesystem.ignored-mount-points "^/(sys|proc|dev|host|etc)($|/)"
```

#安装alertmanager 告警系统

```shell
docker run -d -p 9093:9093 --restart always --name alertmanager \
-v /opt/prometheus/alertmanager/alertmanager.yml:/etc/alertmanager/alertmanager.yml \
-v /opt/prometheus/alertmanager:/alertmanager \
-v /opt/prometheus/alertmanager/template/email.tmpl:/usr/local/alertmanager/template/email.tmpl \
prom/alertmanager
```

安装grafana 展示面板

```shell
docker run -d --restart=always  -p 3000:3000   --name=grafana   -v /opt/prometheus/grafana-storage:/var/lib/grafana   grafana/grafana
```



安装portainer，用于更简单的查看docker的各个镜像是使用情况，查询logs等

```shell
docker run -d -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v /opt/prometheus/portainer_data:/data portainer/portainer-ce:2.11.0
```

安装blackbox 黑盒监控插件

```shell
docker run -d -p 9115:9115 \
--name blackbox_exporter \
-v /usr/share/zoneinfo/Asia/Shanghai:/etc/localtime:ro \
-v /opt/prometheus/blackbox/blackbox.yml:/config/blackbox.yml \
prom/blackbox-exporter --config.file=/config/blackbox.yml
```

安装influxdb，可省略也可使用，将prometheus的后端数据存储到数据库中

```shell
docker run -d \
    --name influxdb2 \
    -p 8086:8086 \
    --volume /opt/prometheus/influxdb:/var/lib/influxdb2 \
	-v /opt/prometheus/influxdb/conf/config.yml:/etc/influxdb2/config.yml \
    influxdb:2.3.0
```

设置influxdb需在prometheus.yml中加上指定数据库

  - ```yaml
    remote_write:
      - url: "http://192.168.0.27:8086/api/v1/prom/write?db=数据库&u=用户名&p=密码"
    remote_read:
      - url: "http://192.168.0.27:8086/api/v1/prom/read?db=数据库&u=用户名&p=密码"
    ```

接下来可以选择自动发现功能，我采用的是大神 StarsL.cn 的ConsulManager开源和其grafana模板,以下内容都是复制的，无脑安装即可

地址：https://github.com/starsliao?tab=repositories

需先安装consul

```shell
# 使用yum部署consul

yum install -y yum-utils
yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo
yum -y install consul

# 或者直接下RPM包安装

wget https://rpm.releases.hashicorp.com/RHEL/7/x86_64/stable/consul-1.12.2-1.x86_64.rpm
rpm -ivh ./consul-1.12.2-1.x86_64.rpm
```

配置

```shell
vi /etc/consul.d/consul.hcl
advertise_addr = "192.168.x.x" #可以先不加这行，如果启动有问题再加上，一般有多网卡需要配置这行，填写你的网卡IP
data_dir = "/opt/consul"
client_addr = "0.0.0.0"
ui_config{
  enabled = true
}
server = true
bootstrap = true
acl = {
  enabled = true
  default_policy = "deny"
  enable_token_persistence = true
}
```

```shell
启动和授权
chown -R consul:consul /opt/consul  #注意下数据目录的权限。
systemctl enable consul.service
systemctl start consul.service
# 获取登录密码
consul acl bootstrap
# 记录 SecretID，即为Consul登录的Token
```

- 下载：`wget https://raw.githubusercontent.com/starsliao/ConsulManager/main/docker-compose.yml`

- 国内下载：`wget https://starsl.cn/static/img/docker-compose.yml`

- 编辑：docker-compose.yml

  ，修改3个环境变量：

  - **`consul_token`**：consul的登录token
  - **`consul_url`**：consul的URL(http开头，/v1要保留)
  - **`admin_passwd`**：登录Consul Manager的admin密码

- 启动：`docker-compose pull && docker-compose up -d`

- 访问：`http://{IP}:1026`，使用配置的Consul Manager密码登录
