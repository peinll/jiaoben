2.配置 hive on spark
1）版本
   spark-2.4.8.tgz（源码） 

2）配置流程

(1) 背景
     hive源码中，依赖的spark版本是2.3.0，该版本spark不支持hadoop3。hadoop3引用的guava版本为19以上，spark2.3.0引用的guava为14，最后报错找不到方法com.google.common.base.Stopwatch

为了兼容hadoop3 环境，spark3支持hadoop3.2和以上版本，但不兼容hive3.1.0，启动查询时报错
NoClassDefFoundError: org/apache/spark/AccumulatorParam 
原因是高版本spark废弃了该类

为了兼容hadoop3和hive3，所以选择了支持hadoop3的低版本spark2，即spark2.4.8，需要手动编译源码支持hadoop3.1

编译spark-2.4.8

./dev/make-distribution.sh --name without-hive --tgz -Pyarn -Phadoop-3.1 -Dhadoop.version=3.1.3 -Pparquet-provided -Porc-provided -Phadoop-provided
编译完成后生成spark-2.4.5-bin-without-hive.tgz文件

 (2) 安装
解压

tar zxvf spark-2.4.5-bin-without-hive.tgz
mv  spark-2.4.5-bin-without-hive spark

配置环境变量

vi /etc/profile.d/spark.sh
export SPARK_HOME=/opt/soft/spark 
export PATH=$PATH:$SPARK_HOME/bin

source /etc/profile

配置spark-defaults.conf

cp spark-defaults.conf.template spark-defaults.conf
vi  spark-defaults.conf

spark-defaults.con内容

spark.master                             yarn
spark.eventLog.enabled                   true
spark.eventLog.dir                       hdfs://localhost:9000/spark-history
spark.executor.memory                    1g
spark.driver.memory                      1g

创建hdfs下spark目录

hadoop fs -mkdir /spark-history

配置spark-env.sh

cp spark-env.sh.template spark-env.sh

#spark-env.sh添加
export SPARK_DIST_CLASSPATH=$(hadoop classpath)

上传spark jar

hadoop fs -mkdir /spark-jars2
hadoop fs -put /spark/jars/* /spark-jars2


修改 hive-site.xml

    <!--Spark依赖位置-->
    <property>
        <name>spark.yarn.jars</name>
        <value>hdfs://localhost:9000/spark-jars/*</value>
    </property>
    <!--Hive执行引擎-->
    <property>
        <name>hive.execution.engine</name>
        <value>spark</value>
    </property>


    <!--Hive和Spark连接超时时间-->
    <property>
        <name>hive.spark.client.connect.timeout</name>
        <value>10000ms</value>
    </property>

3）参数配置
(1）方法一，在sql中添加

#设置计算引擎
set hive.execution.engine=spark;


#设置spark提交模式
set spark.master=yarn-cluster;


#设置作业提交队列
set spark.yarn.queue=queue_name;


#设置队列的名字
set mapreduce.job.queuename=root.users.hdfs;


#设置作业名称
set spark.app.name=job_name;


#该参数用于设置Spark作业总共要用多少个Executor进程来执行
set spark.executor.instances=25;


#设置执行器计算核心个数
set spark.executor.cores=4;


#设置执行器内存
set spark.executor.memory=8g


#设置任务并行度
set mapred.reduce.tasks=600;


#设置每个executor的jvm堆外内存
set spark.yarn.executor.memoryOverhead=2048;


#设置内存比例(spark2.0+)
set spark.memory.fraction=0.8;


#设置对象序列化方式
set spark.serializer=org.apache.serializer.KyroSerializer;  
（2）方法2，修改hive-site.xml

    <property>
        <name>spark.home</name>
        <value>/opt/soft/spark</value>
    </property>
    <property>
        <name>spark.master</name>
        <value>yarn-cluster</value>
    </property>
    <property>
        <name>hive.spark.client.channel.log.level</name>
        <value>WARN</value>
    </property>
    <property>
        <name>spark.eventLog.enabled</name>
        <value>true</value>
    </property>
    <property>
        <name>spark.eventLog.dir</name>
        <value>hdfs://hadoop22:8020/user/hive/tmp/sparkeventlog</value>
    </property>
    <property>
        <name>spark.executor.memory</name>
        <value>1g</value>
    </property>
    <property>
        <name>spark.executor.cores</name>
        <value>2</value>
    </property>
    <property>
        <name>spark.executor.instances</name>
        <value>6</value>
    </property>
    <property>
        <name>spark.yarn.executor.memoryOverhead</name>
        <value>150m</value>
    </property>
    <property>
        <name>spark.driver.memory</name>
        <value>4g</value>
    </property>
    <property>
        <name>spark.yarn.driver.memoryOverhead</name>
        <value>400m</value>
    </property>
    <property>
        <name>spark.serializer</name>
        <value>org.apache.spark.serializer.KryoSerializer</value>
    </property>
    <property>
        <name>spark.yarn.jars</name>
        <value>hdfs://hadoop22:8020/spark-jars/*</value>
    </property>

其他参考https://www.jianshu.com/p/d8b18f0a28c7


如果遇到sqoop导入报错ERROR tool.ImportTool: Import failed: java.io.IOException: Hive exited with status 64

cp hive-common-3.1.2.jar /opt/soft/sqoop/lib/
cp hive-exec-3.1.2.jar /opt/soft/sqoop/lib/
cp mysql-connector-java-8.0.23.jar /opt/soft/sqoop/lib/
cp libthrift-0.9.3.jar /opt/soft/sqoop/lib/