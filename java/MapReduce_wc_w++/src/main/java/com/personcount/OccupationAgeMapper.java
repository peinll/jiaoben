package com.personcount;

import com.utils.PersonInfo;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.IOException;

// Mapper类（简化版，使用字符串拼接
//）
public class OccupationAgeMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
    private final static IntWritable one = new IntWritable(1);
    private Text occupation = new Text();

    public void map(LongWritable key, Text value, Context context
    ) throws IOException, InterruptedException {
        // 假设输入行使用逗号分隔
        String[] parts = value.toString().split(",");
        if (parts.length == 9) {
            PersonInfo personInfo = new PersonInfo();
            personInfo.setProvinceCity(new Text(parts[0]));
            personInfo.setDistrict(new Text(parts[1]));
            personInfo.setLocation(new Text(parts[2]));
            personInfo.setCode(new Text(parts[3]));
            personInfo.setPhoneNumber(new Text(parts[4]));
            personInfo.setName(new Text(parts[5]));
            personInfo.setAge(Integer.parseInt(parts[6]));
            personInfo.setGender(Integer.parseInt(parts[7]));
            personInfo.setOccupation(new Text(parts[8]));


            // 提取职业字段（假设是最后一个字段）
            String occupationStr = parts[8].trim();
            occupation.set(occupationStr); // 设置键为职业
            // 发出键值对 (职业, 1)
            context.write(occupation, one); // 正确的调用，键是Text类型，值是IntWritable类型
        }
    }
}
