package com.OccupationPartitioner;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class OccupationPartitioner extends Partitioner<Text, IntWritable> {
    @Override
    public int getPartition(Text key, IntWritable value, int numReduceTasks) {
        String occupation = key.toString();
        if ("未知".equalsIgnoreCase(occupation)) {
            return 0;
        }else {
            return 1;
        }
    }
}
