package com.utils;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class PersonInfo implements WritableComparable<PersonInfo> {

    private Text provinceCity;       // 省市
    private Text district;          // 区县
    private Text location;          // 具体地点
    private Text code;              // 编码/密钥
    private Text phoneNumber;       // 联系电话号码
    private Text name;             // 姓名
    private int age;                // 年龄
    private int gender;             // 性别标识
    private Text occupation;        // 职业或备注

    public PersonInfo() {
        // 必须有默认构造函数供反射使用
        provinceCity = new Text();
        district = new Text();
        location = new Text();
        code = new Text();
        phoneNumber = new Text();
        name = new Text();
        age = 0;
        gender = 0;
        occupation = new Text();
    }

    // Getter and Setter methods for each field...

    public Text getProvinceCity() { return provinceCity; }
    public void setProvinceCity(Text provinceCity) { this.provinceCity = provinceCity; }

    public Text getDistrict() { return district; }
    public void setDistrict(Text district) { this.district = district; }

    public Text getLocation() { return location; }
    public void setLocation(Text location) { this.location = location; }

    public Text getCode() { return code; }
    public void setCode(Text code) { this.code = code; }

    public Text getPhoneNumber() { return phoneNumber; }
    public void setPhoneNumber(Text phoneNumber) { this.phoneNumber = phoneNumber; }

    public Text getName() { return name; }
    public void setName(Text name) { this.name = name; }

    public int getAge() { return age; }
    public void setAge(int age) { this.age = age; }

    public int getGender() { return gender; }
    public void setGender(int gender) { this.gender = gender; }

    public Text getOccupation() { return occupation; }
    public void setOccupation(Text occupation) { this.occupation = occupation; }

    // 实现Writable接口的方法

    @Override
    public void write(DataOutput out) throws IOException {
        provinceCity.write(out);
        district.write(out);
        location.write(out);
        code.write(out);
        phoneNumber.write(out);
        name.write(out);
        out.writeInt(age);
        out.writeInt(gender);
        occupation.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        provinceCity.readFields(in);
        district.readFields(in);
        location.readFields(in);
        code.readFields(in);
        phoneNumber.readFields(in);
        name.readFields(in);
        age = in.readInt();
        gender = in.readInt();
        occupation.readFields(in);
    }

    // 实现Comparable接口的方法，定义排序逻辑

    @Override
    public int compareTo(PersonInfo other) {
        int cmp = this.provinceCity.compareTo(other.provinceCity);
        if (cmp != 0) return cmp;
        cmp = this.district.compareTo(other.district);
        if (cmp != 0) return cmp;
        cmp = this.location.compareTo(other.location);
        if (cmp != 0) return cmp;
        // 可以继续添加更多字段进行排序，这里只演示到location
        return cmp;
    }

    // 重写toString方法，方便打印和调试

    @Override
    public String toString() {
        return provinceCity + "," + district + "," + location + "," + code + "," + phoneNumber + "," + name + "," + age + "," + gender + "," + occupation;
    }
}