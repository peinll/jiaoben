package com.personAvgAge;

import com.sun.xml.internal.xsom.impl.scd.Iterators;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import java.io.IOException;

public class AvgAgeReducer extends Reducer<Text, IntWritable, Text, DoubleWritable> {

    private DoubleWritable result = new DoubleWritable();
    private int sum = 0;
    private int count = 0;
    public void reduce(Text key, Iterable<IntWritable> values,
                       Context context) throws IOException, InterruptedException {
        sum = 0;
        count = 0;
        for (IntWritable val : values){
            sum += val.get();
            count++;
        }
        if (count != 0){
            int averageAge = Math.round((float) sum / count);
            result.set(averageAge);
            context.write(key, result);
        }
    }

}
