package com.personAvgAge;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.IOException;

public class avgAgeMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
    private final static IntWritable one = new IntWritable(1);
//    private Text occupationAge = new Text();

    public void map(LongWritable key, Text value, Context context
    ) throws IOException, InterruptedException {
        String[] person = value.toString().split(",");
        if (person.length >=9){
            String occupation  = person[8].trim();
            int age = Integer.parseInt(person[6].trim());
//            occupationAge.set(occupation  + "_" + age);
            context.write(new Text(occupation), new IntWritable(age));
        }
    }
}
