#!/bin/bash

host=hadoop

for ip in 22 23 24
do
	ssh $host$ip hadoop-daemon.sh start journalnode
	sleep 1
done

ssh hadoop22 sh /opt/soft/hadoop/sbin/start-all.sh
sleep 2s
ssh hadoop22 mapred --daemon start historyserver
