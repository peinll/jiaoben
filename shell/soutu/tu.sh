#!/bin/bash

date=$(date +%Y%m%d)
echo $date
dir=/opt/souTu/out
#year= `date +%Y`
#echo $date

#图包 
echo '图包img1开始检索'
find  /data/www/img1/image2022/ -mtime -7 -size +1M -type f -exec ls -l {} \; > $dir/img1-1M-$date.txt
echo '图包结束'
#小图 
echo 'imgs开始检索'
find /data/www/imgs/pic/2022 -mtime -7 -size +100k -type f -exec ls -l {} \; > $dir/imgs-100k-$date.txt
echo 'imgs结束'
#幻灯 
echo 'image开始检索'
find  /data/www/image/gameshd/2022/ -mtime -7 -size +200k -type f -exec ls -l {} \; > $dir/gameshd-200k-$date.txt
echo 'image结束'

#修改img1
echo '修改img1'
img1old='/data/www/img1'
img1new='http://img1.abcd.com'
fileName=img1-1M-$date.txt

sed -i -e "s!$img1old!$img1new!g" $dir/$fileName

#修改image
echo '修改image'
gameshdold='/data/www/image'
gameshdnew='http://image.abcd.com'
fileNameimage=gameshd-200k-$date.txt

sed -i -e "s!$gameshdold!$gameshdnew!g" $dir/$fileNameimage

#修改imgs
echo '修改imgs'
imgsold='/data/www/imgs'
imgsnew='http://imgs.abcd.com'
fileNameimgs=imgs-100k-$date.txt

sed -i -e "s!$imgsold!$imgsnew!g" $dir/$fileNameimgs

# echo $(date -d -5day +%Y%m%d)


#查找是否有和上周相同的
dateD7=$(date -d -7day +%Y%m%d)

#img1
echo '查找img1中与上周相同的'
fileNameD7=img1-1M-$dateD7.txt
echo $fileName
echo $fileNameD7
# sed -i -e "s!$img1old!$img1new!g" $dir/$fileNameD7
#echo $fileName
cat /opt/souTu/out\/$fileName | while read line
do
    url1=$(echo $line | awk '{print $9}')
    #echo $url1
    cat /opt/souTu/out\/$fileNameD7 | while read line2
    do
        url2=$(echo $line2 | awk '{print $9}')
        # echo $url1'--'$url2
        if [ "$url1" = "$url2" ]; then
            echo $url1
            #sed '\/$url1/d' out\/$fileName
        fi
    done
done

#fileNameimage
echo '查找image中与上周相同的'
fileNameimageD7=gameshd-200k-$dateD7.txt
echo $fileNameimage
echo $fileNameimageD7
cat /opt/souTu/out\/$fileNameimage | while read line
do
    url1=$(echo $line | awk '{print $9}')
    #echo $url1
    cat /opt/souTu/out\/$fileNameimageD7 | while read line2
    do
        url2=$(echo $line2 | awk '{print $9}')
        # echo $url1'--'$url2
        if [ "$url1" = "$url2" ]; then
            echo $url1
            #sed '\/$url1/d' out\/$fileName
        fi
    done
done

#fileNameimgs
echo '查找imgs中与上周相同的'
fileNameimgsD7=imgs-100k-$dateD7.txt
echo $fileNameimgs
echo $fileNameimgsD7
# sed -i -e "s!$img1old!$img1new!g" $dir/$fileNameD7
# echo $fileName
cat /opt/souTu/out\/$fileNameimgs| while read line
do
    url1=$(echo $line | awk '{print $9}')
    #echo $url1
    cat /opt/souTu/out\/$fileNameimgsD7 | while read line2
    do
        url2=$(echo $line2 | awk '{print $9}')
        # echo $url1'--'$url2
        if [ "$url1" = "$url2" ]; then
            echo $url1
            #sed '\/$url1/d' out\/$fileName
        fi
    done
done

echo  > /opt/souTu/index.txt
echo "==================$fileName===========" >> /opt/souTu/index.txt
cat /opt/souTu/out\/$fileName >> /opt/souTu/index.txt
echo "==================$fileNameimgs===========" >> /opt/souTu/index.txt
cat /opt/souTu/out\/$fileNameimgs >> /opt/souTu/index.txt
echo "==================$fileNameimage===========" >> /opt/souTu/index.txt
cat /opt/souTu/out\/$fileNameimage >> /opt/souTu/index.txt
