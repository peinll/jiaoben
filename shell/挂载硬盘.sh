#!/bin/bash

function create_mount_dir() {
  mkdir /appdata
}

#重做数据盘文件系统
function create_filesystem() {
  mkfs.xfs /dev/sdb
}
#查询文件系统的uuid
function generate_fstab_entry() {
  uuid=$(lsblk | grep -i sdb | awk '{print $2}' | tr -d '"')

  echo "UUID=$uuid /appdata xfs defaults 0 0" >> /etc/fstab
}

#查看lsblk中是否有sdb数据盘
if lsblk | grep -i sdb; then
  create_mount_dir
  create_filesystem 
  generate_fstab_entry
fi

#挂载检查
mount -a
if [ $? -eq 0 ]; then
    echo "挂载成功"
else
    echo "挂载失败"
fi
#确认挂载成功
# 获取df -h命令的输出  
df_output=$(df -h)  
  
# 使用grep查找包含"sdb"和"appdata"的行  
if $(df -h | grep -q "/dev/sdb.*appdata"); then  
  echo "成功"  
fi
