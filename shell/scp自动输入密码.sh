#!/usr/bin/expect  
  
# 设置超时时间  
set timeout 30  
  
# 配置SSH连接参数  
set host "server_ip"  
set user "your_username"  
set password "your_password"  
set remote_file "/path/to/remote/file"  
set local_file "/path/to/local/file"  
  
# 启动SSH会话  
spawn scp ${user}@${host}:${remote_file} ${local_file}  
  
# 等待SSH提示符  
expect {  
  "*assword:" {  
    # 发送密码  
    send "${password}\r"  
    exp_continue  
  }  
  timeout {  
    # 超时处理  
    puts "SSH连接超时，请检查网络和服务器状态！"  
    exit 1  
  }  
}  
  
# 等待文件传输完成  
expect eof