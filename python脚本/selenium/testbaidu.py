from time import sleep
from selenium.webdriver.common.by import By
# from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options

from selenium import webdriver

# 调用键盘按键操作时需要引入的Keys包
from selenium.webdriver.common.keys import Keys

# 不自动关闭浏览器
option = webdriver.ChromeOptions()
option.add_experimental_option("detach", True)

Chrome = webdriver.Chrome(executable_path="./chromedriver.exe",options=option)#启动chromedriver
#隐式等待
Chrome.implicitly_wait(10)

Chrome.get('http://www.baidu.com')#打开http://www.baidu.com

# Chrome.find_element(By.XPATH,'//*[@id="s-top-loginbtn"]').click()#点击登录按钮
# Chrome.find_element(By.XPATH,'//*[@id="kw"]').send_keys("35435435")#点击登录按钮
# Chrome.find_element(By.XPATH,'//*[@id="su"]').click()
Chrome.find_element(By.ID,'kw').send_keys("123123")
Chrome.find_element(By.ID,'su').click()
# Chrome.quit()

# sleep(10000)