import subprocess
import os

# 获取当前目录的文件列表
video_list = os.listdir()

for file in video_list:
 # 如果文件是mkv视频
 #if file.endswith('.mkv'):
 if file.endswith('.mp4'):
       
 # 定义ffmpeg命令,提取内嵌字幕到当前目录下同名的srt文件中
        #导出字幕
        #cmd = f'ffmpeg.exe -i "{file}" "{os.path.splitext(file)[0]}.srt"'
        #格式转换
        #cmd = f'ffmpeg.exe -i "{file}" -vcodec copy -acodec copy "{os.path.splitext(file)[0]}.mp4"'
        #嵌入字幕
        cmd = f'ffmpeg.exe -i "{file}" -vf "subtitles={os.path.splitext(file)[0]}.srt" "cut\cut_{file}"'
 # 执行ffmpeg命令
        subprocess.run(cmd, shell=True)