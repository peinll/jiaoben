# -*- coding: utf-8 -*-
import os
import re
import time
import sys
from datetime import datetime

import pymysql

reload(sys)
sys.setdefaultencoding('utf8')


def del_None(None_list):
    return [i for i in None_list if i]


def find_keyword(log):
    # 正则表达式模式，用于提取执行时间
    date_pattern = r"\[(.*?)\]|"
    sql_pattern = r"SQL\[(.*?)\]"
    exec_time_pattern = r"TotalExecTime\[(.*?)us\]"
    begin_time_pattern = r"BeginTs\[(.*?)\]"
    end_time_pattern = r"EndTs\[(.*?)\]"
    userName_pattern = r"UserName\[(.*?)\]"
    Database_pattern = r"Database\[(.*?)\]"

    # 使用 re.findall() 方法查找所有匹配项
    # date_times = [dt for dt in re.findall(date_pattern, log) if dt]
    # sql_statements = [dt for dt in re.findall(sql_pattern, log) if dt]
    # exec_times = [dt for dt in re.findall(exec_time_pattern, log) if dt]
    # begin_times = [dt for dt in re.findall(begin_time_pattern, log) if dt]
    # end_times = [dt for dt in re.findall(end_time_pattern, log) if dt]
    # Database = [dt for dt in re.findall(Database_pattern, log) if dt]

    date_times = del_None(re.findall(date_pattern, log))
    # print date_times
    sql_statements = re.findall(sql_pattern, log)
    exec_times = del_None(re.findall(exec_time_pattern, log))
    # exec_times_s = float(exec_times[0]) * 0.000001
    begin_times = re.findall(begin_time_pattern, log)
    end_times = re.findall(end_time_pattern, log)
    user_name = re.findall(userName_pattern, log)
    Database = re.findall(Database_pattern, log)
    # 如果提取到的数据为空，则返回 None

    if not sql_statements:
        # print("SQL:", sql_statements[0])
        return None
    # 输出提取的 SQL 语句和执行时间
    # print "Date:", date_times[0]
    original_datetime = datetime.strptime(date_times[0], '%Y-%m-%d %H:%M:%S:%f')
    date_times = original_datetime.strftime('%Y-%m-%d %H:%M:%S')
    # print "exec_times(s):", exec_times[0]
    # print "begin_times:", begin_times[0]
    # print "end_times:", end_times[0]
    # print "UserName:", user_name[0]
    # print "Database:", Database[0]
    # print "SQL:", sql_statements[0]
    # print("---------------------")
    if not Database[0]:
        Database[0] = 'None'
    ZZZstr = r'"%s"@ZZZ"%s"@ZZZ"%s"@ZZZ"%s"@ZZZ"%s"@ZZZ"%s"@ZZZ"%s"' % (
        str(date_times), str(exec_times[0]), str(begin_times[0]), str(end_times[0]), str(user_name[0]),
        str(Database[0]),
        str(sql_statements[0]))
    a_list = [date_times, int(exec_times[0]), begin_times[0], end_times[0], user_name[0], Database[0],
              sql_statements[0]]
    array_list = [ZZZstr, a_list]

    # str
    return array_list


def read_sql(logs, current_datetimes):
    out_dir = 'out/'
    # 检查目录是否存在，如果不存在则创建
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    query_file_name = logs.split('\\')[-1].split('.')[0]
    if '/' in query_file_name:
        query_file_name = query_file_name.split('/')[-1]
    # print query_file_name
    # 每个日志是多行为一基础,需要检索日期开头到下个日期为止的文本为一条日志
    log_list = []
    sql_listes = []
    with open(logs, 'r') as file:
        log_content = ''
        for line in file:
            if re.match(r'\[\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}:\d{3}\]', line):
                if log_content:
                    log_list.append(' '.join(log_content.split()))  # 将上一条合并后的日志内容添加到列表中
                date_time = line[:25]  # 提取日期时间部分
                log_content = date_time + ' ' + line[25:].strip()  # 开始收集新的日志内容
            else:
                log_content += ' ' + line.strip()  # 继续收集当前日志的内容并添加空格分隔
        if log_content:
            log_list.append(' '.join(log_content.split()))  # 将最后一条合并后的日志内容添加到列表中
            for log in log_list:
                # print log
                data = find_keyword(log)
                if data is not None:
                    # 插入一个函数,把输出的data写到mysql中
                    sql_listes.append(data[1])
                    # file_name = 'slow_query-{}-{}.txt'.format(query_file_name, current_datetimes)
                    file_name = os.path.join(out_dir,
                                             'slow_query-{}_{}.csv'.format(query_file_name, current_datetimes))
                    with open(file_name, 'a') as f:
                        f.write(data[0])
                        f.write('\n')
    return sql_listes


if __name__ == '__main__':
    # 获取命令行参数
    script_name = sys.argv[0]
    file_names = sys.argv[1:]
    current_datetime = time.strftime('%Y%m%d%H%M%S')

    try:
        # 连接数据库
        db = pymysql.connect(host='10.10.10.10', user='root', password='qwer1234', port=8880, db='test')
        cursor = db.cursor()
        insert_query = """
    INSERT INTO log_data (timestamp, exec_times, begin_times, end_times,user_name, database_name, sql_statements) values (%s,%s,%s,%s,%s,%s,%s)
        """

        # sql_lists = read_sql(r'sqls\session.log', current_datetime)
        # cursor.executemany(insert_query, sql_lists)

        for file_name in file_names:
            sql_lists = read_sql(file_name, current_datetime)
            # print sql_lists
            cursor.executemany(insert_query, sql_lists)
        db.commit()
        db.close()
    except pymysql.Error as e:
        print("Error: %s" % e)

        print ("connection_to_library_failed,_write_to_csv;")
        for file_name in file_names:
            sql_lists = read_sql(file_name, current_datetime)
            # print sql_lists

    # 当所有操作完成后，关闭所有连接（通常在程序结束时执行）
    # for conn in pool.connections:
    #     conn.close()
    # read_sql(r'sqls\session.log', current_datetime)
    # read_sql(r'sqls\slow_query_19_1.log', current_datetime)
    # read_sql(r'sqls\slow_query_19_2.log', current_datetime)
    # read_sql(r'sqls\slow_query_21_1.log', current_datetime)
    # read_sql(r'sqls\slow_query_21_2.log', current_datetime)
    # read_sql(r'sqls\slow001.log', current_datetime)
