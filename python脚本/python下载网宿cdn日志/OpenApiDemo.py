#!/usr/bin/python
"""
	注释:本脚本只针对Python2.x
	Remark: The scripts is only worked for python2.x 
"""
import datetime
import linecache
import os
import ssl
import sys
import urllib
import urllib.request
from hashlib import sha256
from urllib import parse
import hmac
import base64
import requests
import json
from datetime import date, timedelta


class OpenApiDemo:
    def getDate(self):
        GMT_FORMAT = '%a, %d %b %Y %H:%M:%S GMT'
        date_gmt = datetime.datetime.utcnow().strftime(GMT_FORMAT)
        return date_gmt

    def getAuth(self, userName, apikey, date):
        signed_apikey = hmac.new(apikey.encode('utf-8'), date.encode('utf-8'), sha256).digest()
        signed_apikey = base64.b64encode(signed_apikey)
        signed_apikey = userName + ":" + signed_apikey.decode()
        signed_apikey = base64.b64encode(signed_apikey.encode('utf-8'))
        return signed_apikey

    def createHeader(self, accept, authStr, date):
        headers = {
            'Date': date,
            'Accept': accept,
            'Content-type': accept,
            'Authorization': 'Basic ' + authStr.decode()
        }
        return headers

    def sendRequest(self, httpUrl, method, httpBodyParams, headers):
        if method.upper() == 'POST':
            resp = requests.post(httpUrl, data=httpBodyParams, headers=headers)
        elif method.upper() == 'GET':
            resp = requests.get(httpUrl, headers=headers)
        # tmp_str = self.printResp(resp)
        # print(tmp_str)
        # return tmp_str
        return resp.text

    # def printResp(self, resp):
    #     headers_post = dict(resp.headers);
    #     # tmp_str = "statusCode:{}\nDate:{}\nContent-Length:{}\nConnection:{}\nx-cnc-request-id:{}\n\n{}".format(
    #     #     resp.status_code,
    #     #     headers_post.get('Date'),
    #     #     headers_post.get('Content-Length'),
    #     #     headers_post.get('Connection'),
    #     #     headers_post.get('x-cnc-request-id'),
    #     #     resp.text)
    #     tmp_str = "{}".format(resp.text)
    #     # print(tmp_str)
    #     return tmp_str

    def get_pic_by_url(folder_path, lists):
        if not os.path.exists(folder_path):
            print("Selected folder not exist, try to create it.")
            os.makedirs(folder_path)
        for url in lists:
            print("Try downloading file: {}".format(url))
            filename = url.split('/')[-1]
            filepath = folder_path + '/' + filename
            if os.path.exists(filepath):
                print("File have already exist. skip")
            else:
                try:
                    urllib.request.urlretrieve(url, filename=filepath)
                except Exception as e:
                    print("Error occurred when downloading file, error message:")
                    print(e)
def fenge(url):
    urlList = url.split('/')
    # print(urlList)
    dir=urlList[7]
    fileName=urlList[8].split('?')[0]
    rest=[]
    rest.append(dir)
    rest.append(fileName)
    return rest

if __name__ == '__main__':
    yesterday = (date.today() + timedelta(days=-2)).strftime("%Y-%m-%d")
    today = date.today()
    tomorrow = (date.today() + timedelta(days= 1)).strftime("%Y-%m-%d")
    print("yesterday"+str(yesterday))
    print("today"+str(today))
    print("tomorrow"+str(tomorrow))

    '''
        输入参数部分 begin
    '''
    userName = 'gamersky'  # 替换成真实账号;Replace it with name you want to type
    apikey = 'abcd-1234'  # 替换成真实账号的apikey;Replace it with key you get
    method = 'POST'  # 填写请求方法post/get;Replace it with method you want to type
    accept = 'application/json'  # 填写返回接收数据模式; Typing the mode returned data
    httpHost = "https://open.chinanetcenter.com"
    httpUri = "/api/report/log/downloadLink"
    httpGetParams = {
        "datefrom": str(yesterday)+"T00:00:00+08:00",
        "dateto": str(today)+"T00:00:00+08:00",
        "type": "fiveminutes"
    }
    httpBodyParamsXML = r'''<?xml version="1.0" encoding="utf-8"?>
							<domain-list>
							<domain-name>www.gamersky.com</domain-name>
							</domain-list>'''
    httpBodyParamsJSON = '["www.gamersky.com"]'
    '''
        输入参数部分  end
    '''
    openApiDemo = OpenApiDemo()
    date = openApiDemo.getDate()  # 获取系统时间; Getting system time
    authStr = openApiDemo.getAuth(userName, apikey, date)  # 获取鉴权参数; Getting parameters of authorization
    headers = openApiDemo.createHeader(accept, authStr, date)  # 获取http头部信息; Getting paramters of http header
    httpUrl = httpHost + httpUri + "?" + parse.urlencode(httpGetParams)
    res = openApiDemo.sendRequest(httpUrl, method, httpBodyParamsXML, headers)

    '''
        整理部分
    '''

    # print(res)
    loads = json.loads(res)
    jsondata = loads["logs"]
    # print(jsondata)
    resultdata = jsondata[0]
    # print(resultdata)
    resultdata2 = resultdata["files"]

    addresslist=[]
    for data in resultdata2:
        addresslist.append(data['logUrl'])
    # print(addresslist)

    for url in addresslist:
        # print(url)
        print("Try downloading file: {}".format(url))
        files = fenge(url)
        dir = 'D:\日志备份\www' + files[0]
        # print(files)
        filepath = files[0] + '/' + files[1]
        if os.path.exists(filepath):
            print("File have already exist. skip")
        else:
            if not os.path.exists(files[0]):
                os.makedirs(files[0])
            try:
                print(filepath)
                urllib.request.urlretrieve(url, filename=filepath)
            except Exception as e:
                print("Error occurred when downloading file, error message:")
                print(e)
