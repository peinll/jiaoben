# -*- coding:utf-8 -*-
import glob
import logging
import os
import re

import pandas as pd


def FileHebing(result):
    # result2 = result
    result2 = result.copy() 

    for file in result:
        # file_DATE = file.split('-')[4]
        # temp = re.sub('[a-zA-Z]', '', file.split('-')[2])
        # temp = re.sub('[\d]', '', temp)

        file_DATE, temp = file.split('-')[4], re.sub('[a-zA-Z\d]', '', file.split('-')[2])

        result2.remove(file)
        if 'AZ' in file:
            data = pd.DataFrame()
            try:
                df = pd.read_csv(file)
            except FileNotFoundError as e:
                print('except:', e)
                continue
            df_data1 = pd.DataFrame(df)
            data = pd.concat([data, df_data1])
            os.remove(file)
            for file2 in result2:
                if file[0:15] == file2[0:15]:
                    df2 = pd.read_csv(file2)
                    df_data2 = pd.DataFrame(df2)
                    data = pd.concat([data, df_data2])
                    os.remove(file2)
                    # result.remove(file2)
            filename = f'性能管理-历史查询-{temp}-AZ合并-{file_DATE}'
            data.to_csv(filename, index=False, encoding='utf-8-sig')


    path = '.'
    extension = 'csv'
    os.chdir(path)
    result = glob.glob('*.{}'.format(extension))
    # print(result)
    return result



#
# list = ['性能管理-历史查询-一期存储性能AZ1-he_cn_weihu-20230111161555.csv',
#         '性能管理-历史查询-一期存储性能AZ2-he_cn_weihu-20230111161555.csv',
#         '性能管理-历史查询-三期周存储性能-he_cn_weihu-20230106175729.csv',
#         '性能管理-历史查询-三期存储性能AZ1-he_cn_weihu-20230111154418.csv',
#         '性能管理-历史查询-三期存储性能AZ2-he_cn_weihu-20230111154418.csv',
#         '性能管理-历史查询-二期周主机性能-he_cn_weihu-20230106175430.csv',
#         '性能管理-历史查询-二期周存储性能AZ1-he_cn_weihu-20230106175729.csv',
#         '性能管理-历史查询-二期周存储性能AZ2-he_cn_weihu-20230106175729.csv',
#         '性能管理-历史查询-二期存储性能AZ1-he_cn_weihu-20230111161621.csv',
#         '性能管理-历史查询-二期存储性能AZ2-he_cn_weihu-20230111161621.csv']
#
#
# FileHebing(list)