import os
import json
from datetime import timedelta, datetime
import logging
import yaml

import numpy as np
import xlwings as xw
import pandas as pd
import Read_File


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
# 创建Handler
# 终端Handler
consoleHandler = logging.StreamHandler()
consoleHandler.setLevel(logging.DEBUG)
# 文件Handler
fileHandler = logging.FileHandler('log.log', mode='w', encoding='UTF-8')
fileHandler.setLevel(logging.NOTSET)
# Formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
consoleHandler.setFormatter(formatter)
fileHandler.setFormatter(formatter)
# 添加到Logger中
logger.addHandler(consoleHandler)
logger.addHandler(fileHandler)

f = open('datas.yaml', 'r', encoding='utf-8')
data = f.read()
x = yaml.load(data, Loader=yaml.FullLoader)

def get_diff_time(pdate, days):
    # pdate_obj = datetime.strptime(pdate,"%Y-%m-%d")  #将字符串时间变为日期对象
    time_gap = timedelta(days=days)  # 时间间隔，可以7天也可以其他天数
    pdate_result = pdate - time_gap  # 相减还是一个对象
    return pdate_result.strftime("%Y-%m-%d")  # 调用strftime 变为字符串返回


def daoru(result):
    read_file1 = {}
    ri = []
    zhou = []
    dateList = []
    ri_file_date = ''
    # Read_File.read_File('性能管理-历史查询-一期周主机查询-he_cn_weihu-20230106175430.csv')
    for file in result:
        ri_file_date = file.split('-')[4][0:8]
        if '边缘' in file:
            continue
        else:
            if '周' in file:
                dateList = []
                zhou.append(Read_File.read_File(file))
                file_date = file.split('-')[4][0:8]
                end_date = datetime.date(pd.to_datetime(file_date, format='%Y-%m-%d', errors='coerce'))
                for day in range(1, 8):
                    start_date = get_diff_time(end_date, day)
                    dateList.append(start_date)

            else:
                ri.append(Read_File.read_File(file))
    # print(dateList)
    logger.debug(dateList)
    read_file1['周数据'] = zhou
    read_file1['日数据'] = ri
    replace = str(read_file1).replace('\'', '\"')
    # json_dumps = json.dumps(replace)
    # print(json_dumps)
    # print(type(json_dumps))
    load_data = json.loads(replace)
    print(load_data)
    print(type(load_data))

    '''
    录入数据
    '''
    try:
        # print("以下为录入数据阶段-=========================分割线============")
        logger.debug("以下为录入数据阶段-=========================分割线============")

        from shutil import copyfile

        prePath = os.getcwd() + "\example.xlsx"
        finPath = os.getcwd() + fr"\Report\{ri_file_date}日报汇总.xlsx"
        copyfile(prePath, finPath)

        # 填表用
        app = xw.App(visible=True, add_book=False)
        # wb = app.books.open(r'.\模板测试.xlsx')
        wb = app.books.open(finPath)
        # 日数据
        sheet_Data = wb.sheets['Data']
        # 清空表
        sheet_Data.range("B5:G5").clear_contents()
        sheet_Data.range("B20:G20").clear_contents()
        sheet_Data.range("B35:G35").clear_contents()
        sheet_Data.range("B10:M10").clear_contents()
        sheet_Data.range("B25:M25").clear_contents()
        sheet_Data.range("B40:M40").clear_contents()
        sheet_Data.range("B15:J15").clear_contents()
        sheet_Data.range("B30:J30").clear_contents()
        sheet_Data.range("B45:J45").clear_contents()

        data_ri = load_data["日数据"]
        # print(data_ri)
        for list in data_ri:
            # print(list)
            print("-------")
            logger.debug('--------------')
            # print(list)
            if '一期主机' in str(list):
                keysa = str(list.keys())
                # print(keysa)
                # print(list['一期主机性能']['所有CPU平均利用率(%)'])
                yi_cpu = list['一期主机性能']['所有CPU平均利用率(%)'].replace('[', '').replace(']', '').replace(' ', '').split(',')
                # yi_cpu = re.sub('\[|\]| ', "",list['一期主机性能']['所有CPU平均利用率(%)']).split(',')
                yi_memorys = list['一期主机性能']['内存平均利用率(%)'].replace('[', '').replace(']', '').replace(' ', '').split(',')
                # yi_memorys = re.sub('\[|\]| ', "",list['一期主机性能']['内存平均利用率(%)']).split(',')
                yi_wang_send = list['一期主机性能']['服务器网口发送速率(Mbps)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                           '').split(
                    ',')
                # yi_wang_send = re.sub('\[|\]| ', "",list['一期主机性能']['服务器网口发送速率(Mbps)']).split(',')
                # yi_wang_res = re.sub('\[|\]| ', "",list['一期主机性能']['服务器网口接收速率(Mbps)']).split(',')
                yi_wang_res = list['一期主机性能']['服务器网口接收速率(Mbps)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                          '').split(',')
                print(yi_cpu)
                print(yi_memorys)
                # print(type(yi_cpu[1]))
                sheet_Data.range('B5').value = float(yi_cpu[1]) / 100
                sheet_Data.range('C5').value = float(yi_cpu[0]) / 100
                sheet_Data.range('D5').value = float(yi_memorys[1]) / 100
                sheet_Data.range('E5').value = float(yi_memorys[0]) / 100
                sheet_Data.range('F5').value = float(yi_wang_send[1])
                sheet_Data.range('G5').value = float(yi_wang_res[1])
                print("一期主机结束")
                logger.debug("一期主机结束")
            if '二期主机' in str(list):
                er_cpus = list['二期主机性能']['所有CPU平均利用率(%)'].replace('[', '').replace(']', '').replace(' ', '').split(',')
                er_memorys = list['二期主机性能']['内存平均利用率(%)'].replace('[', '').replace(']', '').replace(' ', '').split(',')
                er_wang_send = list['二期主机性能']['服务器网口发送速率(Mbps)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                           '').split(
                    ',')
                er_wang_res = list['二期主机性能']['服务器网口接收速率(Mbps)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                          '').split(',')
                # 二期主机
                sheet_Data.range('B20').value = float(er_cpus[1]) / 100
                sheet_Data.range('C20').value = float(er_cpus[0]) / 100
                sheet_Data.range('D20').value = float(er_memorys[1]) / 100
                sheet_Data.range('E20').value = float(er_memorys[0]) / 100
                sheet_Data.range('F20').value = float(er_wang_send[1])
                sheet_Data.range('G20').value = float(er_wang_res[1])
                logger.debug("二期主机结束")
            if '三期主机' in str(list):
                third_cpus = list['三期主机性能']['所有CPU平均利用率(%)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                third_memorys = list['三期主机性能']['内存平均利用率(%)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                third_wang_send = list['三期主机性能']['服务器网口发送速率(Mbps)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                              '').split(
                    ',')
                third_wang_res = list['三期主机性能']['服务器网口接收速率(Mbps)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                             '').split(
                    ',')
                # 三期主机
                sheet_Data.range('B35').value = float(third_cpus[1]) / 100
                sheet_Data.range('C35').value = float(third_cpus[0]) / 100
                sheet_Data.range('D35').value = float(third_memorys[1]) / 100
                sheet_Data.range('E35').value = float(third_memorys[0]) / 100
                sheet_Data.range('F35').value = float(third_wang_send[1])
                sheet_Data.range('G35').value = float(third_wang_res[1])
                logger.debug("三期主机结束")
            if '一期虚机' in str(list):
                yix_vcpus = list['一期虚机性能']['虚拟机内部CPU利用率(%)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                yix_memorys = list['一期虚机性能']['虚拟机内部内存利用率(%)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                yix_hw_r = list['一期虚机性能']['该虚拟机所有虚拟硬盘读出速率的总和(MB/s)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                               '').split(
                    ',')
                yix_hw_w = list['一期虚机性能']['该虚拟机所有虚拟硬盘写入速率的总和(MB/s)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                               '').split(
                    ',')
                yix_iops_r = list['一期虚机性能']['该虚拟机所有虚拟硬盘读出IOPS的总和'].replace('[', '').replace(']', '').replace(' ',
                                                                                                             '').split(
                    ',')
                yix_iops_w = list['一期虚机性能']['该虚拟机所有虚拟硬盘写入IOPS的总和'].replace('[', '').replace(']', '').replace(' ',
                                                                                                             '').split(
                    ',')
                yix_net_send = list['一期虚机性能']['该虚拟机所有虚拟端口发送速率的总和(Mbps)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                                   '').split(
                    ',')
                yix_net_res = list['一期虚机性能']['该虚拟机所有虚拟端口接收速率的总和(Mbps)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                                  '').split(
                    ',')
                # 一期虚机
                sheet_Data.range('B10').value = x['1qi']['vCPU总个数']
                sheet_Data.range('C10').value = float(yix_vcpus[1]) / 100
                sheet_Data.range('D10').value = float(yix_vcpus[0]) / 100
                sheet_Data.range('E10').value = x['1qi']['内存大小TB']
                sheet_Data.range('F10').value = float(yix_memorys[1]) / 100
                sheet_Data.range('G10').value = float(yix_memorys[0]) / 100
                sheet_Data.range('H10').value = np.abs(float(yix_hw_r[1]))
                sheet_Data.range('I10').value = np.abs(float(yix_hw_w[1]))
                sheet_Data.range('J10').value = np.abs(float(yix_iops_r[1]))
                sheet_Data.range('K10').value = float(yix_iops_w[1])
                sheet_Data.range('L10').value = float(yix_net_send[1])
                sheet_Data.range('M10').value = float(yix_net_res[1])
                logger.debug("一期虚机结束")
            if '二期虚机' in str(list):
                erx_vcpus = list['二期虚机性能']['虚拟机内部CPU利用率(%)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                erx_memorys = list['二期虚机性能']['虚拟机内部内存利用率(%)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                erx_hw_r = list['二期虚机性能']['该虚拟机所有虚拟硬盘读出速率的总和(MB/s)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                               '').split(
                    ',')
                erx_hw_w = list['二期虚机性能']['该虚拟机所有虚拟硬盘写入速率的总和(MB/s)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                               '').split(
                    ',')
                erx_iops_r = list['二期虚机性能']['该虚拟机所有虚拟硬盘读出IOPS的总和'].replace('[', '').replace(']', '').replace(' ',
                                                                                                             '').split(
                    ',')
                erx_iops_w = list['二期虚机性能']['该虚拟机所有虚拟硬盘写入IOPS的总和'].replace('[', '').replace(']', '').replace(' ',
                                                                                                             '').split(
                    ',')
                erx_net_send = list['二期虚机性能']['该虚拟机所有虚拟端口发送速率的总和(Mbps)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                                   '').split(
                    ',')
                erx_net_res = list['二期虚机性能']['该虚拟机所有虚拟端口接收速率的总和(Mbps)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                                  '').split(
                    ',')
                # 二期虚机
                sheet_Data.range('B25').value = x['2qi']['vCPU总个数']
                sheet_Data.range('C25').value = float(erx_vcpus[1]) / 100
                sheet_Data.range('D25').value = float(erx_vcpus[0]) / 100
                sheet_Data.range('E25').value = x['2qi']['内存大小TB']
                sheet_Data.range('F25').value = float(erx_memorys[1]) / 100
                sheet_Data.range('G25').value = float(erx_memorys[0]) / 100
                sheet_Data.range('H25').value = float(erx_hw_r[1])
                sheet_Data.range('I25').value = float(erx_hw_w[1])
                sheet_Data.range('J25').value = float(erx_iops_r[1])
                sheet_Data.range('K25').value = float(erx_iops_w[1])
                sheet_Data.range('L25').value = float(erx_net_send[1])
                sheet_Data.range('M25').value = float(erx_net_res[1])
                logger.debug("二期虚机结束")
            if '三期虚机' in str(list):
                erx_vcpus = list['三期虚机性能']['虚拟机内部CPU利用率(%)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                erx_memorys = list['三期虚机性能']['虚拟机内部内存利用率(%)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                erx_hw_r = list['三期虚机性能']['该虚拟机所有虚拟硬盘读出速率的总和(MB/s)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                               '').split(
                    ',')
                erx_hw_w = list['三期虚机性能']['该虚拟机所有虚拟硬盘写入速率的总和(MB/s)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                               '').split(
                    ',')
                erx_iops_r = list['三期虚机性能']['该虚拟机所有虚拟硬盘读出IOPS的总和'].replace('[', '').replace(']', '').replace(' ',
                                                                                                             '').split(
                    ',')
                erx_iops_w = list['三期虚机性能']['该虚拟机所有虚拟硬盘写入IOPS的总和'].replace('[', '').replace(']', '').replace(' ',
                                                                                                             '').split(
                    ',')
                erx_net_send = list['三期虚机性能']['该虚拟机所有虚拟端口发送速率的总和(Mbps)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                                   '').split(
                    ',')
                erx_net_res = list['三期虚机性能']['该虚拟机所有虚拟端口接收速率的总和(Mbps)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                                  '').split(
                    ',')
                # 三期虚机
                sheet_Data.range('B40').value = x['3qi']['vCPU总个数']
                sheet_Data.range('C40').value = float(erx_vcpus[1]) / 100
                sheet_Data.range('D40').value = float(erx_vcpus[0]) / 100
                sheet_Data.range('E40').value = x['3qi']['内存大小TB']
                sheet_Data.range('F40').value = float(erx_memorys[1]) / 100
                sheet_Data.range('G40').value = float(erx_memorys[0]) / 100
                sheet_Data.range('H40').value = float(erx_hw_r[1])
                sheet_Data.range('I40').value = float(erx_hw_w[1])
                sheet_Data.range('J40').value = float(erx_iops_r[1])
                sheet_Data.range('K40').value = float(erx_iops_w[1])
                sheet_Data.range('L40').value = float(erx_net_send[1])
                sheet_Data.range('M40').value = float(erx_net_res[1])
                logger.debug("三期虚机结束")
            if '一期存储' in str(list):
                yic_jq_zong = list['一期存储性能']['分布式块存储集群总容量(GB)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                          '').split(',')
                yic_jq_fenpei = list['一期存储性能']['分布式块存储集群已分配容量(GB)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                              '').split(
                    ',')
                yic_c_fenpei = list['一期存储性能']['分布式块存储池已分配容量(GB)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                            '').split(
                    ',')
                yic_c_r = list['一期存储性能']['分布式块存储池读速率(MB/s)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                yic_c_w = list['一期存储性能']['分布式块存储池写速率(MB/s)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                yic_c_iopsr = list['一期存储性能']['分布式块存储池读IOPS'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                yic_c_iopsw = list['一期存储性能']['分布式块存储池写IOPS'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                yic_c_io = list['一期存储性能']['分布式块存储池平均IO时延(ms)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                yic_jq_yiyong = list['一期存储性能']['分布式块存储集群已用容量(GB)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                             '').split(
                    ',')

                # 一期存储
                sheet_Data.range('B15').value = float(yic_jq_zong[2]) / 1024
                sheet_Data.range('C15').value = float(yic_jq_fenpei[2]) / 1024
                sheet_Data.range('D15').value = float(yic_c_fenpei[2]) / 1024
                sheet_Data.range('E15').value = np.abs(float(yic_c_r[1]))
                sheet_Data.range('F15').value = float(yic_c_w[1])
                sheet_Data.range('G15').value = float(yic_c_iopsr[1])
                sheet_Data.range('H15').value = float(yic_c_iopsw[1])
                sheet_Data.range('I15').value = np.abs(float(yic_c_io[1]))
                sheet_Data.range('J15').value = float(yic_jq_yiyong[2])
                logger.debug("一期存储结束")
            if '二期存储' in str(list):
                erc_jq_zong = list['二期存储性能']['分布式块存储集群总容量(GB)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                          '').split(',')
                erc_jq_fenpei = list['二期存储性能']['分布式块存储集群已分配容量(GB)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                              '').split(
                    ',')
                erc_c_fenpei = list['二期存储性能']['分布式块存储池已分配容量(GB)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                            '').split(
                    ',')
                erc_c_r = list['二期存储性能']['分布式块存储池读速率(MB/s)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                erc_c_w = list['二期存储性能']['分布式块存储池写速率(MB/s)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                erc_c_iopsr = list['二期存储性能']['分布式块存储池读IOPS'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                erc_c_iopsw = list['二期存储性能']['分布式块存储池写IOPS'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                erc_c_io = list['二期存储性能']['分布式块存储池平均IO时延(ms)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                erc_jq_yiyong = list['二期存储性能']['分布式块存储集群已用容量(GB)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                             '').split(
                    ',')

                # 二期存储
                sheet_Data.range('B30').value = float(erc_jq_zong[2]) / 1024
                sheet_Data.range('C30').value = float(erc_jq_fenpei[2]) / 1024
                sheet_Data.range('D30').value = float(erc_c_fenpei[2]) / 1024
                sheet_Data.range('E30').value = float(erc_c_r[1])
                sheet_Data.range('F30').value = float(erc_c_w[1])
                sheet_Data.range('G30').value = float(erc_c_iopsr[1])
                sheet_Data.range('H30').value = float(erc_c_iopsw[1])
                sheet_Data.range('I30').value = float(erc_c_io[1])
                sheet_Data.range('J30').value = float(erc_jq_yiyong[2])
                logger.debug("二期存储结束")
            if '三期存储' in str(list):
                thirdc_jq_zong = list['三期存储性能']['分布式块存储集群总容量(GB)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                             '').split(
                    ',')
                thirdc_jq_fenpei = list['三期存储性能']['分布式块存储集群已分配容量(GB)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                                 '').split(
                    ',')
                thirdc_c_fenpei = list['三期存储性能']['分布式块存储池已分配容量(GB)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                               '').split(
                    ',')
                thirdc_c_r = list['三期存储性能']['分布式块存储池读速率(MB/s)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                          '').split(',')
                thirdc_c_w = list['三期存储性能']['分布式块存储池写速率(MB/s)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                          '').split(',')
                thirdc_c_iopsr = list['三期存储性能']['分布式块存储池读IOPS'].replace('[', '').replace(']', '').replace(' ',
                                                                                                          '').split(',')
                thirdc_c_iopsw = list['三期存储性能']['分布式块存储池写IOPS'].replace('[', '').replace(']', '').replace(' ',
                                                                                                          '').split(',')
                thirdc_c_io = list['三期存储性能']['分布式块存储池平均IO时延(ms)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                            '').split(
                    ',')
                thirdc_jq_yiyong = list['三期存储性能']['分布式块存储集群已用容量(GB)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                                '').split(
                    ',')

                # 三期存储
                sheet_Data.range('B45').value = float(thirdc_jq_zong[2]) / 1024
                sheet_Data.range('C45').value = float(thirdc_jq_fenpei[2]) / 1024
                sheet_Data.range('D45').value = float(thirdc_c_fenpei[2]) / 1024
                sheet_Data.range('E45').value = float(thirdc_c_r[1])
                sheet_Data.range('F45').value = float(thirdc_c_w[1])
                sheet_Data.range('G45').value = float(thirdc_c_iopsr[1])
                sheet_Data.range('H45').value = float(thirdc_c_iopsw[1])
                sheet_Data.range('I45').value = float(thirdc_c_io[1])
                sheet_Data.range('J45').value = float(thirdc_jq_yiyong[2])
                logger.debug("三期存储结束")

        # 周数据
        logger.debug("进入周数据环节")
        sheet_Data = wb.sheets['DataWK']
        data_zhou = load_data["周数据"]

        riqi = []
        for list in data_zhou:
            if '一期周主机' in str(list):
                b = []
                for key in dateList:
                    print(key)
                    yi_z_cpu = list.get('一期周主机性能').get(key).get('所有CPU平均利用率(%)')[1] / 100
                    yi_z_memorys = list.get('一期周主机性能').get(key).get('内存平均利用率(%)')[1] / 100
                    yi_z_net_send = list.get('一期周主机性能').get(key).get('服务器网口发送速率(Mbps)')[1]
                    yi_z_net_res = list.get('一期周主机性能').get(key).get('服务器网口接收速率(Mbps)')[1]

                    a = [key, yi_z_cpu, yi_z_memorys, yi_z_net_send, yi_z_net_res]
                    b.append(a)
                print(b)
                logger.debug(b)
                sheet_Data.range("A34:E40").clear_contents()
                sheet_Data.range('A34:E40').value = b
            if '二期周主机' in str(list):
                b = []
                for key in dateList:
                    # print(key)
                    yi_z_cpu = list.get('二期周主机性能').get(key).get('所有CPU平均利用率(%)')[1] / 100
                    yi_z_memorys = list.get('二期周主机性能').get(key).get('内存平均利用率(%)')[1] / 100
                    yi_z_net_send = list.get('二期周主机性能').get(key).get('服务器网口发送速率(Mbps)')[1]
                    yi_z_net_res = list.get('二期周主机性能').get(key).get('服务器网口接收速率(Mbps)')[1]

                    a = [key, yi_z_cpu, yi_z_memorys, yi_z_net_send, yi_z_net_res]
                    b.append(a)
                print(b)
                logger.debug(b)
                sheet_Data.range("A102:E108").clear_contents()

                sheet_Data.range('A102:E108').value = b
            if '三期周主机' in str(list):
                b = []
                for key in dateList:
                    # print(key)
                    yi_z_cpu = list.get('三期周主机性能').get(key).get('所有CPU平均利用率(%)')[1] / 100
                    yi_z_memorys = list.get('三期周主机性能').get(key).get('内存平均利用率(%)')[1] / 100
                    yi_z_net_send = list.get('三期周主机性能').get(key).get('服务器网口发送速率(Mbps)')[1]
                    yi_z_net_res = list.get('三期周主机性能').get(key).get('服务器网口接收速率(Mbps)')[1]

                    a = [key, yi_z_cpu, yi_z_memorys, yi_z_net_send, yi_z_net_res]
                    b.append(a)
                print(b)
                logger.debug(b)
                sheet_Data.range("A174:E180").clear_contents()

                sheet_Data.range("A174:E180").value = b
            if '一期周虚机' in str(list):
                b = []
                for key in dateList:
                    # print(key)
                    yi_z_cpu = list.get('一期周虚机性能').get(key).get('虚拟机内部CPU利用率(%)')[1] / 100
                    yi_z_memorys = list.get('一期周虚机性能').get(key).get('虚拟机内部内存利用率(%)')[1] / 100
                    hd_r = list.get('一期周虚机性能').get(key).get('该虚拟机所有虚拟硬盘读出速率的总和(MB/s)')[1]
                    hd_w = list.get('一期周虚机性能').get(key).get('该虚拟机所有虚拟硬盘写入速率的总和(MB/s)')[1]
                    iops_r = list.get('一期周虚机性能').get(key).get('该虚拟机所有虚拟硬盘读出IOPS的总和')[1]
                    iops_w = list.get('一期周虚机性能').get(key).get('该虚拟机所有虚拟硬盘写入IOPS的总和')[1]
                    net_send = list.get('一期周虚机性能').get(key).get('该虚拟机所有虚拟端口发送速率的总和(Mbps)')[1]
                    net_res = list.get('一期周虚机性能').get(key).get('该虚拟机所有虚拟端口接收速率的总和(Mbps)')[1]
                    a = [key, yi_z_cpu, yi_z_memorys, hd_r, hd_w, iops_r, iops_w, net_send, net_res]
                    b.append(a)
                print(b)
                logger.debug(b)
                sheet_Data.range("A46:I52").clear_contents()
                sheet_Data.range('A46:I52').value = b
            if '二期周虚机' in str(list):
                b = []
                for key in dateList:
                    # print(key)
                    yi_z_cpu = list.get('二期周虚机性能').get(key).get('虚拟机内部CPU利用率(%)')[1] / 100
                    yi_z_memorys = list.get('二期周虚机性能').get(key).get('虚拟机内部内存利用率(%)')[1] / 100
                    hd_r = list.get('二期周虚机性能').get(key).get('该虚拟机所有虚拟硬盘读出速率的总和(MB/s)')[1]
                    hd_w = list.get('二期周虚机性能').get(key).get('该虚拟机所有虚拟硬盘写入速率的总和(MB/s)')[1]
                    iops_r = list.get('二期周虚机性能').get(key).get('该虚拟机所有虚拟硬盘读出IOPS的总和')[1]
                    iops_w = list.get('二期周虚机性能').get(key).get('该虚拟机所有虚拟硬盘写入IOPS的总和')[1]
                    net_send = list.get('二期周虚机性能').get(key).get('该虚拟机所有虚拟端口发送速率的总和(Mbps)')[1]
                    net_res = list.get('二期周虚机性能').get(key).get('该虚拟机所有虚拟端口接收速率的总和(Mbps)')[1]
                    a = [key, yi_z_cpu, yi_z_memorys, hd_r, hd_w, iops_r, iops_w, net_send, net_res]
                    b.append(a)
                print(b)
                logger.debug(b)
                sheet_Data.range("A116:I122").clear_contents()
                sheet_Data.range('A116:I122').value = b
            if '三期周虚机' in str(list):
                b = []
                for key in dateList:
                    # print(key)
                    yi_z_cpu = list.get('三期周虚机性能').get(key).get('虚拟机内部CPU利用率(%)')[1] / 100
                    yi_z_memorys = list.get('三期周虚机性能').get(key).get('虚拟机内部内存利用率(%)')[1] / 100
                    hd_r = list.get('三期周虚机性能').get(key).get('该虚拟机所有虚拟硬盘读出速率的总和(MB/s)')[1]
                    hd_w = list.get('三期周虚机性能').get(key).get('该虚拟机所有虚拟硬盘写入速率的总和(MB/s)')[1]
                    iops_r = list.get('三期周虚机性能').get(key).get('该虚拟机所有虚拟硬盘读出IOPS的总和')[1]
                    iops_w = list.get('三期周虚机性能').get(key).get('该虚拟机所有虚拟硬盘写入IOPS的总和')[1]
                    net_send = list.get('三期周虚机性能').get(key).get('该虚拟机所有虚拟端口发送速率的总和(Mbps)')[1]
                    net_res = list.get('三期周虚机性能').get(key).get('该虚拟机所有虚拟端口接收速率的总和(Mbps)')[1]
                    a = [key, yi_z_cpu, yi_z_memorys, hd_r, hd_w, iops_r, iops_w, net_send, net_res]
                    b.append(a)
                print(b)
                logger.debug(b)
                sheet_Data.range("A188:I194").clear_contents()
                sheet_Data.range('A188:I194').value = b

            if '一期周存储' in str(list):
                b = []
                b1 = []
                for key in dateList:
                    # print(key)
                    yi_z_cpu = list.get('一期周存储性能').get(key).get('分布式块存储集群总容量(GB)')[2] / 1024
                    yi_z_memorys = list.get('一期周存储性能').get(key).get('分布式块存储集群已分配容量(GB)')[2] / 1024
                    hd_r = list.get('一期周存储性能').get(key).get('分布式块存储池已分配容量(GB)')[2] / 1024
                    hd_w = list.get('一期周存储性能').get(key).get('分布式块存储池读速率(MB/s)')[1]
                    iops_r = list.get('一期周存储性能').get(key).get('分布式块存储池写速率(MB/s)')[1]
                    iops_w = list.get('一期周存储性能').get(key).get('分布式块存储池读IOPS')[1]
                    net_send = list.get('一期周存储性能').get(key).get('分布式块存储池写IOPS')[1]
                    net_res = list.get('一期周存储性能').get(key).get('分布式块存储池平均IO时延(ms)')[1]
                    jq_yiyong = list.get('一期周存储性能').get(key).get('分布式块存储集群已用容量(GB)')[2]
                    a = [key, yi_z_cpu, yi_z_memorys, hd_r, hd_w, iops_r, iops_w, net_send, net_res, jq_yiyong]
                    b.append(a)

                    name = x['1qi']['name']
                    changjia = x['1qi']['changjia']
                    vcpu = x['1qi']['vCPU总个数']
                    cpubili = x['1qi']['CPU分配比例%']
                    memzong = x['1qi']['内存大小TB']
                    membili = x['1qi']['内存分配比例%']
                    shili = x['1qi']['虚拟机实例数']
                    netnum = x['1qi']['虚拟网络个数']
                    a1 = [key, name, changjia, vcpu, cpubili, 0, memzong, membili, 0, yi_z_cpu, yi_z_memorys, 0, shili,
                          netnum]
                    b1.append(a1)
                print(b)
                logger.debug(b)
                sheet_Data.range("A5:N11").clear_contents()
                sheet_Data.range('A5:N11').value = b1
                sheet_Data.range("A60:J66").clear_contents()
                sheet_Data.range('A60:J66').value = b
            if '二期周存储' in str(list):
                b = []
                b1 = []
                for key in dateList:
                    # print(key)
                    yi_z_cpu = list.get('二期周存储性能').get(key).get('分布式块存储集群总容量(GB)')[2] / 1024
                    yi_z_memorys = list.get('二期周存储性能').get(key).get('分布式块存储集群已分配容量(GB)')[2] / 1024
                    hd_r = list.get('二期周存储性能').get(key).get('分布式块存储池已分配容量(GB)')[2] / 1024
                    hd_w = list.get('二期周存储性能').get(key).get('分布式块存储池读速率(MB/s)')[1]
                    iops_r = list.get('二期周存储性能').get(key).get('分布式块存储池写速率(MB/s)')[1]
                    iops_w = list.get('二期周存储性能').get(key).get('分布式块存储池读IOPS')[1]
                    net_send = list.get('二期周存储性能').get(key).get('分布式块存储池写IOPS')[1]
                    net_res = list.get('二期周存储性能').get(key).get('分布式块存储池平均IO时延(ms)')[1]
                    jq_yiyong = list.get('二期周存储性能').get(key).get('分布式块存储集群已用容量(GB)')[2]
                    a = [key, yi_z_cpu, yi_z_memorys, hd_r, hd_w, iops_r, iops_w, net_send, net_res, jq_yiyong]
                    b.append(a)

                    name = x['2qi']['name']
                    changjia = x['2qi']['changjia']
                    vcpu = x['2qi']['vCPU总个数']
                    cpubili = x['2qi']['CPU分配比例%']
                    memzong = x['2qi']['内存大小TB']
                    membili = x['2qi']['内存分配比例%']
                    shili = x['2qi']['虚拟机实例数']
                    netnum = x['2qi']['虚拟网络个数']
                    a1 = [key, name, changjia, vcpu, cpubili, 0, memzong, membili, 0, yi_z_cpu, yi_z_memorys, 0, shili,
                          netnum]
                    b1.append(a1)

                print(b)
                logger.debug(b)
                sheet_Data.range("A74:N80").clear_contents()
                sheet_Data.range('A74:N80').value = b1
                sheet_Data.range("A130:J136").clear_contents()
                sheet_Data.range('A130:J136').value = b
            if '三期周存储' in str(list):
                b = []
                b1 = []
                for key in dateList:
                    # print(key)
                    yi_z_cpu = list.get('三期周存储性能').get(key).get('分布式块存储集群总容量(GB)')[2] / 1024
                    yi_z_memorys = list.get('三期周存储性能').get(key).get('分布式块存储集群已分配容量(GB)')[2] / 1024
                    hd_r = list.get('三期周存储性能').get(key).get('分布式块存储池已分配容量(GB)')[2] / 1024
                    hd_w = list.get('三期周存储性能').get(key).get('分布式块存储池读速率(MB/s)')[1]
                    iops_r = list.get('三期周存储性能').get(key).get('分布式块存储池写速率(MB/s)')[1]
                    iops_w = list.get('三期周存储性能').get(key).get('分布式块存储池读IOPS')[1]
                    net_send = list.get('三期周存储性能').get(key).get('分布式块存储池写IOPS')[1]
                    net_res = list.get('三期周存储性能').get(key).get('分布式块存储池平均IO时延(ms)')[1]
                    jq_yiyong = list.get('三期周存储性能').get(key).get('分布式块存储集群已用容量(GB)')[2]
                    a = [key, yi_z_cpu, yi_z_memorys, hd_r, hd_w, iops_r, iops_w, net_send, net_res, jq_yiyong]
                    b.append(a)

                    name = x['3qi']['name']
                    changjia = x['3qi']['changjia']
                    vcpu = x['3qi']['vCPU总个数']
                    cpubili = x['3qi']['CPU分配比例%']
                    memzong = x['3qi']['内存大小TB']
                    membili = x['3qi']['内存分配比例%']
                    shili = x['3qi']['虚拟机实例数']
                    netnum = x['3qi']['虚拟网络个数']
                    a1 = [key, name, changjia, vcpu, cpubili, 0, memzong, membili, 0, yi_z_cpu, yi_z_memorys, 0, shili,
                          netnum]
                    b1.append(a1)

                print(b)
                logger.debug(b)
                sheet_Data.range("A147:N153").clear_contents()
                sheet_Data.range('A147:N153').value = b1
                sheet_Data.range("A202:J208").clear_contents()
                sheet_Data.range('A202:J208').value = b

        # wb.save(fr'.\Report\{file_date}日报汇总.xlsx')
        wb.save()
        wb.close()
        app.quit()
    except TypeError:
        print("TypeError: 'NoneType' object is not subscriptable 数据缺失")