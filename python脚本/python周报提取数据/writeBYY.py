import os
import json
from datetime import timedelta, datetime
import logging
import yaml

import numpy as np
import xlwings as xw
import pandas as pd
import Read_File

f = open('datas.yaml', 'r', encoding='utf-8')
data = f.read()
x = yaml.load(data, Loader=yaml.FullLoader)


def get_diff_time(pdate, days):
    # pdate_obj = datetime.strptime(pdate,"%Y-%m-%d")  #将字符串时间变为日期对象
    time_gap = timedelta(days=days)  # 时间间隔，可以7天也可以其他天数
    pdate_result = pdate - time_gap  # 相减还是一个对象
    return pdate_result.strftime("%Y-%m-%d")  # 调用strftime 变为字符串返回


def daoru(result):
    read_file1 = {}
    ri = []
    zhou = []
    dateList = []
    ri_file_date = ''
    # Read_File.read_File('性能管理-历史查询-一期周主机查询-he_cn_weihu-20230106175430.csv')
    for file in result:
        ri_file_date = file.split('-')[4][0:8]
        if '期' in file:
            continue
        elif '周' in file:
            dateList = []
            zhou.append(Read_File.read_File(file))
            file_date = file.split('-')[4][0:8]
            end_date = datetime.date(pd.to_datetime(file_date, format='%Y-%m-%d', errors='coerce'))
            for day in range(1,8):
                start_date = get_diff_time(end_date, day)
                dateList.append(start_date)
        else:
            ri.append(Read_File.read_File(file))

    read_file1['周数据'] = zhou   
    read_file1['日数据'] = ri
    replace = str(read_file1).replace('\'', '\"')
    # json_dumps = json.dumps(replace)
    # print(json_dumps)
    # print(type(json_dumps))
    load_data = json.loads(replace)
    print(load_data)
    print(type(load_data))

    '''
    录入数据
    '''

    try:

        print("以下为录入数据阶段-=========================分割线============")

        from shutil import copyfile

        prePath = os.getcwd() + r"\byyexample.xlsx"
        finPath = os.getcwd() + fr"\Report\边缘云{ri_file_date}指标汇总.xlsx"
        copyfile(prePath, finPath)
        # 填表用
        app = xw.App(visible=True, add_book=False)
        # wb = app.books.open(r'.\模板测试.xlsx')
        wb = app.books.open(finPath)
        # 日数据
        sheet_Data = wb.sheets['Data']
        # 清空表
        sheet_Data.range("B5:G5").clear_contents()
        sheet_Data.range("B10:M10").clear_contents()
        sheet_Data.range("B15:J15").clear_contents()

        data_ri = load_data["日数据"]
        # print(data_ri)
        for list in data_ri:
            # print(list)
            print("-------")
            if '主机' in str(list):
                keysa = str(list.keys())
                yi_cpu = list['边缘云主机性能']['所有CPU平均利用率(%)'].replace('[', '').replace(']', '').replace(' ', '').split(',')
                yi_memorys = list['边缘云主机性能']['内存平均利用率(%)'].replace('[', '').replace(']', '').replace(' ', '').split(',')
                yi_wang_send = list['边缘云主机性能']['服务器网口发送速率(Mbps)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                           '').split(
                    ',')
                yi_wang_res = list['边缘云主机性能']['服务器网口接收速率(Mbps)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                          '').split(',')
                print(yi_cpu)
                print(yi_memorys)
                sheet_Data.range('B5').value = float(yi_cpu[1]) / 100
                sheet_Data.range('C5').value = float(yi_cpu[0]) / 100
                sheet_Data.range('D5').value = float(yi_memorys[1]) / 100
                sheet_Data.range('E5').value = float(yi_memorys[0]) / 100
                sheet_Data.range('F5').value = float(yi_wang_send[1])
                sheet_Data.range('G5').value = float(yi_wang_res[1])
                print("边缘云主机结束")
            if '虚机' in str(list):
                yix_vcpus = list['边缘云虚机性能']['虚拟机内部CPU利用率(%)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                yix_memorys = list['边缘云虚机性能']['虚拟机内部内存利用率(%)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                yix_hw_r = list['边缘云虚机性能']['该虚拟机所有虚拟硬盘读出速率的总和(MB/s)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                               '').split(
                    ',')
                yix_hw_w = list['边缘云虚机性能']['该虚拟机所有虚拟硬盘写入速率的总和(MB/s)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                               '').split(
                    ',')
                yix_iops_r = list['边缘云虚机性能']['该虚拟机所有虚拟硬盘读出IOPS的总和'].replace('[', '').replace(']', '').replace(' ',
                                                                                                             '').split(
                    ',')
                yix_iops_w = list['边缘云虚机性能']['该虚拟机所有虚拟硬盘写入IOPS的总和'].replace('[', '').replace(']', '').replace(' ',
                                                                                                             '').split(
                    ',')
                yix_net_send = list['边缘云虚机性能']['该虚拟机所有虚拟端口发送速率的总和(Mbps)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                                   '').split(
                    ',')
                yix_net_res = list['边缘云虚机性能']['该虚拟机所有虚拟端口接收速率的总和(Mbps)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                                  '').split(
                    ',')
                # 一期虚机
                sheet_Data.range('B10').value = x['byy']['vCPU总个数']
                sheet_Data.range('C10').value = float(yix_vcpus[1]) / 100
                sheet_Data.range('D10').value = float(yix_vcpus[0]) / 100
                sheet_Data.range('E10').value = x['byy']['内存大小TB']
                sheet_Data.range('F10').value = float(yix_memorys[1]) / 100
                sheet_Data.range('G10').value = float(yix_memorys[0]) / 100
                sheet_Data.range('H10').value = np.abs(float(yix_hw_r[1]))
                sheet_Data.range('I10').value = np.abs(float(yix_hw_w[1]))
                sheet_Data.range('J10').value = np.abs(float(yix_iops_r[1]))
                sheet_Data.range('K10').value = float(yix_iops_w[1])
                sheet_Data.range('L10').value = float(yix_net_send[1])
                sheet_Data.range('M10').value = float(yix_net_res[1])
                print("边缘云虚机结束")
            if '存储' in str(list):
                yic_jq_zong = list['边缘云存储性能']['分布式块存储集群总容量(GB)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                          '').split(',')
                yic_jq_fenpei = list['边缘云存储性能']['分布式块存储集群已分配容量(GB)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                              '').split(
                    ',')
                yic_c_fenpei = list['边缘云存储性能']['分布式块存储池已分配容量(GB)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                            '').split(
                    ',')
                yic_c_r = list['边缘云存储性能']['分布式块存储池读速率(MB/s)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                yic_c_w = list['边缘云存储性能']['分布式块存储池写速率(MB/s)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                yic_c_iopsr = list['边缘云存储性能']['分布式块存储池读IOPS'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                yic_c_iopsw = list['边缘云存储性能']['分布式块存储池写IOPS'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                yic_c_io = list['边缘云存储性能']['分布式块存储池平均IO时延(ms)'].replace('[', '').replace(']', '').replace(' ', '').split(
                    ',')
                yic_jq_yiyong = list['边缘云存储性能']['分布式块存储集群已用容量(GB)'].replace('[', '').replace(']', '').replace(' ',
                                                                                                             '').split(
                    ',')

                # 一期存储
                sheet_Data.range('B15').value = float(yic_jq_zong[2]) / 1024
                sheet_Data.range('C15').value = float(yic_jq_fenpei[2]) / 1024
                sheet_Data.range('D15').value = float(yic_c_fenpei[2]) / 1024
                sheet_Data.range('E15').value = np.abs(float(yic_c_r[1]))
                sheet_Data.range('F15').value = float(yic_c_w[1])
                sheet_Data.range('G15').value = float(yic_c_iopsr[1])
                sheet_Data.range('H15').value = np.abs(float(yic_c_iopsw[1]))
                sheet_Data.range('I15').value = np.abs(float(yic_c_io[1]))
                sheet_Data.range('J15').value = float(yic_jq_yiyong[2])
                print("边缘云存储结束")

        # 周数据
        print("进入周数据环节")
        sheet_Data = wb.sheets['DataWK']
        data_zhou = load_data["周数据"]

        sheet_Data.range("A34:E40").clear_contents()
        sheet_Data.range("A46:I52").clear_contents()
        sheet_Data.range("A5:N11").clear_contents()
        sheet_Data.range("A60:J66").clear_contents()

        riqi = []
        for list in data_zhou:
            if '周主机' in str(list):
                b = []
                for key in dateList:
                    print(key)
                    yi_z_cpu = list.get('边缘云周主机性能').get(key).get('所有CPU平均利用率(%)')[1] / 100
                    yi_z_memorys = list.get('边缘云周主机性能').get(key).get('内存平均利用率(%)')[1] / 100
                    yi_z_net_send = list.get('边缘云周主机性能').get(key).get('服务器网口发送速率(Mbps)')[1]
                    yi_z_net_res = list.get('边缘云周主机性能').get(key).get('服务器网口接收速率(Mbps)')[1]

                    a = [key, yi_z_cpu, yi_z_memorys, yi_z_net_send, yi_z_net_res]
                    b.append(a)
                print(b)
                sheet_Data.range('A34:E40').value = b
            if '周虚机' in str(list):
                b = []
                for key in dateList:
                    # print(key)
                    yi_z_cpu = list.get('边缘云周虚机性能').get(key).get('虚拟机内部CPU利用率(%)')[1] / 100
                    yi_z_memorys = list.get('边缘云周虚机性能').get(key).get('虚拟机内部内存利用率(%)')[1] / 100
                    hd_r = list.get('边缘云周虚机性能').get(key).get('该虚拟机所有虚拟硬盘读出速率的总和(MB/s)')[1]
                    hd_w = list.get('边缘云周虚机性能').get(key).get('该虚拟机所有虚拟硬盘写入速率的总和(MB/s)')[1]
                    iops_r = list.get('边缘云周虚机性能').get(key).get('该虚拟机所有虚拟硬盘读出IOPS的总和')[1]
                    iops_w = list.get('边缘云周虚机性能').get(key).get('该虚拟机所有虚拟硬盘写入IOPS的总和')[1]
                    net_send = list.get('边缘云周虚机性能').get(key).get('该虚拟机所有虚拟端口发送速率的总和(Mbps)')[1]
                    net_res = list.get('边缘云周虚机性能').get(key).get('该虚拟机所有虚拟端口接收速率的总和(Mbps)')[1]
                    a = [key, yi_z_cpu, yi_z_memorys, np.abs(hd_r), np.abs(hd_w), np.abs(iops_r), np.abs(iops_w), net_send, net_res]
                    b.append(a)
                print(b)
                sheet_Data.range('A46:I52').value = b
            if '周存储' in str(list):
                b = []
                b1 = []
                for key in dateList:
                    # print(key)
                    yi_z_cpu = list.get('边缘云周存储性能').get(key).get('分布式块存储集群总容量(GB)')[2] / 1024
                    yi_z_memorys = list.get('边缘云周存储性能').get(key).get('分布式块存储集群已分配容量(GB)')[2] / 1024
                    hd_r = list.get('边缘云周存储性能').get(key).get('分布式块存储池已分配容量(GB)')[2] / 1024
                    hd_w = list.get('边缘云周存储性能').get(key).get('分布式块存储池读速率(MB/s)')[1]
                    iops_r = list.get('边缘云周存储性能').get(key).get('分布式块存储池写速率(MB/s)')[1]
                    iops_w = list.get('边缘云周存储性能').get(key).get('分布式块存储池读IOPS')[1]
                    net_send = list.get('边缘云周存储性能').get(key).get('分布式块存储池写IOPS')[1]
                    net_res = list.get('边缘云周存储性能').get(key).get('分布式块存储池平均IO时延(ms)')[1]
                    jq_yiyong = list.get('边缘云周存储性能').get(key).get('分布式块存储集群已用容量(GB)')[2]
                    a = [key, yi_z_cpu, yi_z_memorys, hd_r, hd_w, iops_r, iops_w, np.abs(net_send), np.abs(net_res), jq_yiyong]
                    b.append(a)

                    name = x['byy']['name']
                    changjia = x['byy']['changjia']
                    vcpu = x['byy']['vCPU总个数']
                    cpubili = x['byy']['CPU分配比例%']
                    memzong = x['byy']['内存大小TB']
                    membili = x['byy']['内存分配比例%']
                    shili = x['byy']['虚拟机实例数']
                    netnum = x['byy']['虚拟网络个数']
                    a1 = [key, name, changjia, vcpu, cpubili, 0, memzong, membili, 0, yi_z_cpu, yi_z_memorys, 0, shili,
                          netnum]
                    b1.append(a1)
                print(b)
                sheet_Data.range('A5:N11').value = b1
                sheet_Data.range('A60:J66').value = b

        # wb.save(fr'.\Report\{file_date}日报汇总.xlsx')
        wb.save()
        wb.close()
        app.quit()
    except TypeError:
        print("TypeError: 'NoneType' object is not subscriptable 数据缺失")
