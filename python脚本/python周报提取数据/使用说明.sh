1.需下载anaconda的python环境包(因为里面已经整合和使用的类包),地址:  https://www.anaconda.com/products/distribution

2.需将被分析文件放置在.py文件同目录

3.文件内容:
	main.py			主执行文件,用来读取csv,复制写入报表
	Read_File.py	用于将文件内容整合为json格式返回给main
	hebing.py		用于将多个相同属性的文件合并为一个文件并删除原来的文件
	datas.yaml		用于填写固定数值指标
	example.xlsx	报表模板,所有最后生成的模板均基于复制于它
	解压缩.bat		顾名思义,批量解压缩并删除游标文件
	requirements.txt	本脚本所包含的依赖包

4.使用时直接双击main.py即可(需把打开方式选为python执行文件)
5.文件名必须为:"性能管理-历史查询-三期周虚机性能(有无AZ均可)-用户-日期标尾.csv"
6.关于合并,源文件格式必须为 "性能管理-历史查询-三期周虚机性能AZX-用户-日期标尾.csv"格式,重点在"AZ",有几个文件就AZ几
7.最后报表将保存在Report文件夹中

8.如果报错为缺少包,则在对应目录中的cmd中执行

pip install -r requirements.txt
或
pip install -r requirements.txt -i http://pypi.douban.com/simple/ --trusted-host pypi.douban.com