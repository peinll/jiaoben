import os
import shutil
from datetime import datetime

dirdate = datetime.now().strftime('%Y%m%d')
os.makedirs(f'/databack/log_backup/{dirdate}/Manager_ip_log', exist_ok=True)
shutil.copytree('/appdata/goldendb/zxmanager/log', f'/databack/log_backup/{dirdate}/Manager_ip_log')
