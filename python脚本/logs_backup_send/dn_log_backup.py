import os
import shutil
from datetime import datetime

dirdate = datetime.now().strftime('%Y%m%d')
src_dir = '/appdata/goldendb/zxdb1/log'
dst_dir = f'/databack/log_backup/{dirdate}/DN_ip_log'

if not os.path.exists(dst_dir):
    os.makedirs(dst_dir)

for file in os.listdir(src_dir):
    if file.endswith('.log'):
        shutil.copy(os.path.join(src_dir, file), dst_dir)
